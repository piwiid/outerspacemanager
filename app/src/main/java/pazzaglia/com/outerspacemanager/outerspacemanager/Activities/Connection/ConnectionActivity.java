package pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.Main.MainActivity;
import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;
import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.SignUp.SubscribeActivity;
import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.SignUp.Class.UserClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConnectionActivity extends AppCompatActivity implements View.OnClickListener {


    public Button btnLogin;
    public EditText textLogin;
    public EditText textPassword;
    public Button btnSubscribe;

    String reponse = null;
    Boolean click = true;

    private ProgressBar spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_login);
        SharedPreferences prefs = getSharedPreferences("token", MODE_PRIVATE);
        String token = prefs.getString("token1", "");

        if(token.length() > 0) {
            Intent myIntent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(myIntent);
        }



        spinner = (ProgressBar)findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);




        textLogin = findViewById(R.id.editTextIdentifiantID);
        textPassword= findViewById(R.id.editTextPasswordID);
        btnLogin = findViewById(R.id.btnValidateID);
        btnSubscribe = findViewById(R.id.btnSubID);

        btnSubscribe.setOnClickListener(this) ;
        btnLogin.setOnClickListener(this) ;
        UserInfo.initInstance();
    }

    @Override
    public void onClick(View v) {
        // CLICK
        switch(v.getId()) {
            case R.id.btnSubID:
                Intent myIntent = new Intent(getApplicationContext(),SubscribeActivity.class);
                startActivity(myIntent);
                break;
            case R.id.btnValidateID:
                Login();
                break;
        }
    }

    public void Connexion() {
        SharedPreferences prefs = getSharedPreferences("token", MODE_PRIVATE);
        String token = prefs.getString("token1", "");
    }
    public void Login() {
        // CLICK

        spinner.setVisibility(View.VISIBLE);
        String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";

        UserClass userClass = new UserClass();
        userClass.UserLogin(textLogin.getText().toString(),textPassword.getText().toString());


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Service service = retrofit.create(Service.class);

        Call<Repo> request = service.loginUser(userClass);

        request.enqueue(new Callback<Repo>() {
            @Override
            public void onResponse(Call<Repo> call, Response<Repo> response) {

                if(response.isSuccessful()) {
                    SharedPreferences settings = getSharedPreferences("token", MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("token1", response.body().getToken());
                    editor.apply();
                    editor.commit();
                    UserInfo.setInstance().setSaveToken(response.body().getToken());
                    Toast.makeText(ConnectionActivity.this, UserInfo.setInstance().getSaveToken(), Toast.LENGTH_LONG).show();


                    Intent myIntent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(myIntent);

                    spinner.setVisibility(View.GONE);

                }


            }

            @Override
            public void onFailure(Call<Repo> call, Throwable t) {
                Toast.makeText(ConnectionActivity.this, "Fail", Toast.LENGTH_LONG).show();
                spinner.setVisibility(View.GONE);


            }
        });

    }
}
