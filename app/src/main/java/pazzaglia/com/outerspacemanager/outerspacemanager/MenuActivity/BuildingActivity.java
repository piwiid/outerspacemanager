package pazzaglia.com.outerspacemanager.outerspacemanager.MenuActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
import pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.building.AdapterBuilding;
import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.RepoBuilding;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by theo on 18/04/2018.
 */

public class BuildingActivity extends Activity implements AdapterView.OnItemClickListener {

    ListView mListView;

    ArrayAdapter<RepoBuilding> adapter;
    String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";
    String SAVE_TOKEN = UserInfo.getInstance().getSaveToken();

    private ProgressBar spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.building_view);

        mListView = (ListView) findViewById(R.id.listView);

        mListView.setOnItemClickListener(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Service service = retrofit.create(Service.class);

        spinner = (ProgressBar)findViewById(R.id.progressBar3);
        spinner.setVisibility(View.VISIBLE);

        Call<Repo> request = service.getListBuilding(SAVE_TOKEN);

        request.enqueue(new Callback<Repo>() {
            @Override
            public void onResponse(Call<Repo> call, Response<Repo> response) {

                if(response.isSuccessful()) {
                    //Toast.makeText(BuildingActivity.this, UserInfo.getInstance().getMinerals(), Toast.LENGTH_LONG).show();
                    //Toast.makeText(BuildingActivity.this, UserInfo.getInstance().getGas(), Toast.LENGTH_LONG).show();
                    adapter = new AdapterBuilding(getApplicationContext(), android.R.layout.simple_list_item_1, response.body().getBuildings());
                    mListView.setAdapter(adapter);
                    spinner.setVisibility(View.GONE);



                } else {
                    Toast.makeText(BuildingActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<Repo> call, Throwable t) {
                Toast.makeText(BuildingActivity.this, "Fail", Toast.LENGTH_LONG).show();


            }
        });


    }

    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        //Toast.makeText(getApplicationContext(), (int) id, Toast.LENGTH_SHORT).show();

        String indexid = String.valueOf(position);



        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Construire ce batîment ?")
                .setMessage("Est-ce que vous voulez construire ce batiment")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Integer itemId = adapter.getItem(position).getBuildingId();

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(apiUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        Service service = retrofit.create(Service.class);
                        Call<Repo> request = service.createBuilding(SAVE_TOKEN, itemId);

                        request.enqueue(new Callback<Repo>() {
                            @Override
                            public void onResponse(Call<Repo> call, Response<Repo> response) {

                                if(response.isSuccessful()) {
                                    Toast.makeText(BuildingActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(BuildingActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<Repo> call, Throwable t) {
                                Toast.makeText(BuildingActivity.this, "Fail", Toast.LENGTH_LONG).show();


                            }
                        });


                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        Toast.makeText(BuildingActivity.this, "Annuler", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }
}
