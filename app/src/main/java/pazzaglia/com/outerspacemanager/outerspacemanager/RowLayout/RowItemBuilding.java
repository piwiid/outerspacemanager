package pazzaglia.com.outerspacemanager.outerspacemanager.RowLayout;

/**
 * Created by theo on 18/04/2018.
 */

public class RowItemBuilding {
    private String image;
    private String name;

    public RowItemBuilding(String image, String name) {
        this.image = image;
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
