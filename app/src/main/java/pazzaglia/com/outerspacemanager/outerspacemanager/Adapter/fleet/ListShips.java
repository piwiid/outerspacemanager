package pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet;

class ListShips {
    private Integer gasCost;
    private String life;
    private String maxAttack;
    private String minAttack;
    private Integer mineralCost;
    private String name;
    private Integer shipId;
    private String shield;
    private String spatioportLevelNeeded;
    private String speed;
    private String timeToBuild;

    public Integer getGasCost() {
        return gasCost;
    }

    public void setGasCost(Integer gasCost) {
        this.gasCost = gasCost;
    }

    public String getLife() {
        return life;
    }

    public void setLife(String life) {
        this.life = life;
    }

    public String getMaxAttack() {
        return maxAttack;
    }

    public void setMaxAttack(String maxAttack) {
        this.maxAttack = maxAttack;
    }

    public String getMinAttack() {
        return minAttack;
    }

    public void setMinAttack(String minAttack) {
        this.minAttack = minAttack;
    }

    public Integer getMineralCost() {
        return mineralCost;
    }

    public void setMineralCost(Integer mineralCost) {
        this.mineralCost = mineralCost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShipId() {
        return shipId;
    }

    public void setShipId(Integer shipId) {
        this.shipId = shipId;
    }

    public String getShield() {
        return shield;
    }

    public void setShield(String shield) {
        this.shield = shield;
    }

    public String getSpatioportLevelNeeded() {
        return spatioportLevelNeeded;
    }

    public void setSpatioportLevelNeeded(String spatioportLevelNeeded) {
        this.spatioportLevelNeeded = spatioportLevelNeeded;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTimeToBuild() {
        return timeToBuild;
    }

    public void setTimeToBuild(String timeToBuild) {
        this.timeToBuild = timeToBuild;
    }
}
