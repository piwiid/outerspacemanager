package pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Fleet;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
import pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet.AdapterFleet;
import pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet.Fleet;
import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class fleetActivity extends Activity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    AdapterFleet adapter;
    String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";
    String SAVE_TOKEN = UserInfo.getInstance().getSaveToken();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fleet_view);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);



        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Service service = retrofit.create(Service.class);

        Call<Fleet> request = service.getShips(SAVE_TOKEN);

        request.enqueue(new Callback<Fleet>() {
            @Override
            public void onResponse(Call<Fleet> call, Response<Fleet> response) {

                if(response.isSuccessful()) {
                    //Toast.makeText(BuildingActivity.this, UserInfo.getInstance().getMinerals(), Toast.LENGTH_LONG).show();
                    //Toast.makeText(fleetActivity.this, response.body().getShips(), Toast.LENGTH_LONG).show();
                    //adapter = new AdapterBuilding(getApplicationContext(), android.R.layout.simple_list_item_1, response.body().getBuildings());
                    //mListView.setAdapter(adapter);
                    adapter = new AdapterFleet(response.body().getShips());
                    mRecyclerView.setAdapter(adapter);
                    //Log.w("2.0 getFeed > Full json res wrapped in gson => ",new Gson().toJson(response));



                } else {
                    Toast.makeText(fleetActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<Fleet> call, Throwable t) {
                Toast.makeText(fleetActivity.this, "Fail", Toast.LENGTH_LONG).show();
            }
        });



    }
    private void initdata() {
        Fleet fleet = new Fleet();

    }
    // ...
}
