package pazzaglia.com.outerspacemanager.outerspacemanager.Repo;

import java.util.List;

import pazzaglia.com.outerspacemanager.outerspacemanager.RowLayout.RowItemBuilding;

/**
 * Created by theo on 18/04/2018.
 */

public class RepoBuildingModel {

    private String size;


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
