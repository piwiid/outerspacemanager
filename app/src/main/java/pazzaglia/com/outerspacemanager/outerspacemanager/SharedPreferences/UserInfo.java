package pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by theo on 18/04/2018.
 */

public class UserInfo extends Application {

    private String SAVE_TOKEN;
    private double minerals;
    private double gas;


    private static UserInfo instance;

    public static void initInstance() {
        if (instance == null) {
            // Create the instance
            instance = new UserInfo();
        }
    }

    public static UserInfo getInstance() {
        return instance;
    }
    public static UserInfo setInstance() {
        return instance;
    }


    private UserInfo() {
    }

    public String getSaveToken() {
        return SAVE_TOKEN;
    }

    public String setSaveToken(String saveToken) {
        this.SAVE_TOKEN = saveToken;
        return saveToken;
    }

    public double getMinerals() {
        return minerals;
    }

    public void setMinerals(double minerals) {
        this.minerals = minerals;
    }

    public double getGas() {
        return gas;
    }

    public void setGas(double gas) {
        this.gas = gas;
    }


    public void UserInfo(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("token", MODE_PRIVATE);
        SAVE_TOKEN = prefs.getString("token1", "");
    }
}