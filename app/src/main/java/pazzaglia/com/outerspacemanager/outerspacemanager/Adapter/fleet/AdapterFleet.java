package pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Fleet.fleetActivity;
import pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet.ViewHolder;
import pazzaglia.com.outerspacemanager.outerspacemanager.MenuActivity.BuildingActivity;
import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AdapterFleet extends RecyclerView.Adapter<ViewHolder> implements View.OnClickListener {

    private List<ListShips> fleetList;

    String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";
    String SAVE_TOKEN = UserInfo.getInstance().getSaveToken();
    View test = null;



    public AdapterFleet(List<ListShips> fleetList) {
        this.fleetList = fleetList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_cell_fleet, parent, false);

        final ViewHolder mViewHolder = new ViewHolder(itemView);

        mViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Integer indexid = mViewHolder.getAdapterPosition();

                test = v;



                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(v.getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(v.getContext());
                }
                builder.setTitle("Construire ce batîment ?")
                        .setMessage("Est-ce que vous voulez construire ce batiment")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Integer itemId = fleetList.get(indexid).getShipId();

                                Log.i("Salut", String.valueOf(itemId));

                                Retrofit retrofit = new Retrofit.Builder()
                                        .baseUrl(apiUrl)
                                        .addConverterFactory(GsonConverterFactory.create())
                                        .build();

                                Service service = retrofit.create(Service.class);
                                HashMap<String, Integer> hashMap = new HashMap<>();
                                hashMap.put("amount",mViewHolder.progressChangedValue);
                                Call<Fleet> request = service.createShip(SAVE_TOKEN, itemId, hashMap);

                                request.enqueue(new Callback<Fleet>() {
                                    @Override
                                    public void onResponse(Call<Fleet> call, Response<Fleet> response) {

                                        if(response.isSuccessful()) {
                                            Toast.makeText(test.getContext(), "Success", Toast.LENGTH_LONG).show();

                                        } else {

                                            Toast.makeText(test.getContext(), "Fail", Toast.LENGTH_LONG).show();

                                            Log.w("2.0 getFeed > Full json res wrapped in gson => ",new Gson().toJson(response));

                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<Fleet> call, Throwable t) {
                                        Toast.makeText(test.getContext(), "Fail", Toast.LENGTH_LONG).show();

                                    }
                                });


                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });

        return mViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListShips fleet = fleetList.get(position);



        Integer moyenGas = 0;
        Integer moyenMineral = 0;

        moyenGas =   (int)UserInfo.getInstance().getGas() / fleet.getGasCost();
        moyenMineral =   (int)UserInfo.getInstance().getMinerals() / fleet.getGasCost();

        if(moyenGas > moyenMineral) {
            holder.seekBar2.setMax(moyenMineral);
        } else {
            holder.seekBar2.setMax(moyenGas);
        }


        holder.seekBar2.setMax(Integer.valueOf(moyenGas));
        holder.seekBar2.setMax(moyenMineral);
        holder.name.setText(fleet.getName());
        holder.priceGas.setText("Prix Gas " + fleet.getGasCost());
        holder.priceMineral.setText("Prix Mineral " + fleet.getMineralCost());
        holder.userMineral.setText("Your Mineral " + (float)Math.round(UserInfo.getInstance().getMinerals()));
        holder.userGas.setText("Your Gas " + (float)Math.round(UserInfo.getInstance().getGas()));








    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return fleetList.size();
    }

    @Override
    public void onClick(View v) {

    }
}