package pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.Main;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
import pazzaglia.com.outerspacemanager.outerspacemanager.MenuActivity.BuildingActivity;
import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;

import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Fleet.fleetActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by theo on 27/03/2018.
 */

public class MainActivity extends Activity implements View.OnClickListener {

    TextView textUsername;
    TextView textPoint;

    private Button[] btn = new Button[6];
    private Button btn_unfocus;
    private int[] btn_id = {R.id.buttonGeneral, R.id.buttonBuilding, R.id.buttonFlotte, R.id.buttonSearch, R.id.buttonChantier, R.id.buttonGalaxy};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        SharedPreferences prefs = getSharedPreferences("token", MODE_PRIVATE);
        String token = prefs.getString("token1", "");
        String SET_TOKEN = UserInfo.getInstance().setSaveToken(token);

        final String getCurrentminerals = "";
        String getCurrentgas = "";


        for(int i = 0; i < btn.length; i++){
            btn[i] = (Button) findViewById(btn_id[i]);
            btn[i].setOnClickListener(this);
        }


        // GET USER

        Toast.makeText(MainActivity.this, UserInfo.getInstance().getSaveToken(), Toast.LENGTH_LONG).show();


        textUsername = findViewById(R.id.textUsernameID);
        textPoint = findViewById(R.id.textPoint);

        loadData();

    }

    public void loadData() {

        String SAVE_TOKEN = UserInfo.getInstance().getSaveToken();
        String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Service service = retrofit.create(Service.class);

        Call<Repo> request = service.getCurrentUser(SAVE_TOKEN);



        request.enqueue(new Callback<Repo>() {
            @Override
            public void onResponse(Call<Repo> call, Response<Repo> response) {

                if(response.isSuccessful()) {
                    textUsername.setText("Pseudo " +response.body().getUsername());
                    textPoint.setText("Score " + response.body().getPoints());
                    UserInfo.setInstance().setMinerals(response.body().getMinerals());
                    UserInfo.setInstance().setGas(response.body().getGas());

                } else {
                    Toast.makeText(MainActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<Repo> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Fail", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onRestart() {
        super.onRestart();
        loadData();

    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonGeneral :

                break;

            case R.id.buttonBuilding :
                Intent myIntent = new Intent(getApplicationContext(),BuildingActivity.class);
                startActivity(myIntent);
                break;

            case R.id.buttonFlotte :
                Intent myIntentFleet = new Intent(getApplicationContext(),fleetActivity.class);
                startActivity(myIntentFleet);
                break;

            case R.id.buttonSearch :

                break;
            case R.id.buttonChantier:

            break;
            case R.id.buttonGalaxy:

            break;
        }
    }
}
