package pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.SignUp;

        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

        import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
        import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.Main.MainActivity;
        import pazzaglia.com.outerspacemanager.outerspacemanager.R;
        import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
        import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;
        import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.SignUp.Class.UserClass;
        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;
        import retrofit2.Retrofit;
        import retrofit2.converter.gson.GsonConverterFactory;

public class SubscribeActivity extends AppCompatActivity implements View.OnClickListener {


    public Button btnSubscribe;
    public EditText textLogin;
    public EditText textPassword;
    public EditText textEmail;
    String reponse = null;

    public static final String MyPREFERENCES = "token" ;
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subscribe_view);


        textLogin = (EditText) findViewById(R.id.editTextIdentifiantID);
        textPassword= (EditText) findViewById(R.id.editTextPasswordID);
        btnSubscribe = (Button) findViewById(R.id.btnValidateID);
        textEmail = (EditText) findViewById(R.id.editTextEmailID);

        btnSubscribe.setOnClickListener(this) ;


    }

    @Override
    public void onClick(View v) {
        // CLICK

        String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";

        UserClass userClass = new UserClass();
        userClass.UserSub(textEmail.getText().toString(), textLogin.getText().toString(),textPassword.getText().toString());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Service service = retrofit.create(Service.class);

        Call<Repo> request = service.createUser(userClass);

        request.enqueue(new Callback<Repo>() {
            @Override
            public void onResponse(Call<Repo> call, Response<Repo> response) {

                if(response.isSuccessful()) {
                    SharedPreferences settings = getSharedPreferences("token", MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("token1", response.body().getToken());
                    editor.apply();
                    editor.commit();
                    UserInfo.setInstance().setSaveToken(response.body().getToken());
                    Intent myIntent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(myIntent);
                } else {
                    Toast.makeText(SubscribeActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<Repo> call, Throwable t) {
                Toast.makeText(SubscribeActivity.this, "Fail", Toast.LENGTH_LONG).show();

            }
        });

    }
}
