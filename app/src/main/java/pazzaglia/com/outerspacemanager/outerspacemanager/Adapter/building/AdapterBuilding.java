package pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.building;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.RepoBuilding;
import pazzaglia.com.outerspacemanager.outerspacemanager.RowLayout.RowItemBuilding;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;

/**
 * Created by theo on 18/04/2018.
 */

public class AdapterBuilding extends ArrayAdapter<RepoBuilding>  {

    private final Context context;
    private final List<RepoBuilding> buildings;

    public AdapterBuilding(Context context, int simple_list_item_1, List<RepoBuilding> buildings) {
        super(context, R.layout.row_building_view, buildings);
        this.context = context;
        this.buildings = buildings;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_building_view, parent, false);

        TextView textView = (TextView) rowView.findViewById(R.id.name);
        TextView textCostView = (TextView) rowView.findViewById(R.id.textCostView);
        TextView textLevelView = (TextView) rowView.findViewById(R.id.textLevelView);
        TextView textRessourceView = (TextView) rowView.findViewById(R.id.textRessourceView);
        ImageView myImageView = (ImageView) rowView.findViewById(R.id.imageView);


        String url = buildings.get(position).getImageUrl();

        Glide.with(getContext())
                .load(url)
                .into(myImageView);

        textView.setText(buildings.get(position).getName());

        //textView.setText("Price : Mineral "+ calcul(position) + " / " + "Gas " + buildings.get(position).getGasCostLevel0());

        if(buildings.get(position).getLevel() == null) {
            textLevelView.setText("En construction");
            textRessourceView.setText((float)Math.round(UserInfo.getInstance().getGas()) + " / " + (float)Math.round(UserInfo.getInstance().getMinerals()));

        } else {
            textCostView.setText(Integer.toString(calculGasByLevel(position, buildings.get(position).getLevel())) + " / " + String.valueOf(calculMineralByLevel(position, buildings.get(position).getLevel())));
            textRessourceView.setText((float)Math.round(UserInfo.getInstance().getGas()) + " / " + (float)Math.round(UserInfo.getInstance().getMinerals()));
            textLevelView.setText("Level " + buildings.get(position).getLevel());
        }

        return rowView;
        //String s = values[position];
    }


    public int calculGasByLevel(Integer position, Integer level) {
        return buildings.get(position).getMineralCostLevel0() + (level * buildings.get(position).getGasCostByLevel());
    }
    public int calculMineralByLevel(Integer position, Integer level) {
        return buildings.get(position).getGasCostLevel0() + (level * buildings.get(position).getGasCostByLevel());
    }
}
