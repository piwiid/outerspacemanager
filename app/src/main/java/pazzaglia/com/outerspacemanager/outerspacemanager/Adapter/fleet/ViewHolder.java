package pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import pazzaglia.com.outerspacemanager.outerspacemanager.API.Service;
import pazzaglia.com.outerspacemanager.outerspacemanager.MenuActivity.BuildingActivity;
import pazzaglia.com.outerspacemanager.outerspacemanager.R;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.SharedPreferences.UserInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    // each data item is just a string in this case

    String apiUrl = "https://outer-space-manager-staging.herokuapp.com/";

    String SAVE_TOKEN = UserInfo.getInstance().getSaveToken();


    public TextView name;
    public TextView priceGas;
    public TextView priceMineral;
    public TextView userMineral;
    public TextView userGas;
    public TextView numberFleet;
    public SeekBar seekBar2;

    private double SeekGas;
    private double SeekMinerals;
    int progressChangedValue = 0;


    public ViewHolder(View view) {
        super(view);
        view.setOnClickListener(this);
        name = (TextView) view.findViewById(R.id.name);
        priceGas = (TextView) view.findViewById(R.id.priceGas);
        priceMineral = (TextView) view.findViewById(R.id.priceMineral);
        userMineral = (TextView) view.findViewById(R.id.userMineral);
        userGas = (TextView) view.findViewById(R.id.userGas);
        numberFleet = (TextView) view.findViewById(R.id.numberFleet);

        seekBar2 = (SeekBar) view.findViewById(R.id.seekBar2);


        seekBar2.setProgress(0); // 50 default progress value
        numberFleet.setText("Construire 0 vaisseau" );


        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
                numberFleet.setText("Construire " + String.valueOf(progressChangedValue) + " vaisseaux");
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("Satlu", String.valueOf(progressChangedValue));
            }
        });
    }

    @Override
    public void onClick(View v) {
        String indexid = String.valueOf(getAdapterPosition());

        Log.d("coucou", "coucou");





        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(v.getContext(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(v.getContext());
        }

    }
}
