package pazzaglia.com.outerspacemanager.outerspacemanager.API;

import java.util.HashMap;

import pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet.Fleet;
import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.Repo;
import pazzaglia.com.outerspacemanager.outerspacemanager.Activities.Connection.SignUp.Class.UserClass;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by theo on 27/03/2018.
 */

public interface Service {
    @POST("api/v1/auth/create")
    Call<Repo> createUser(@Body UserClass userClass);

    @POST("/api/v1/auth/login")
    Call<Repo> loginUser(@Body UserClass userClass);

    @GET("api/v1/users/get")
    Call<Repo> getCurrentUser(@Header("x-access-token") String token);

    @GET("/api/v1/buildings/list")
    Call<Repo> getListBuilding(@Header("x-access-token") String token);

    @GET("/api/v1/ships")
    Call<Fleet> getShips(@Header("x-access-token") String token);

    @POST("/api/v1/ships/create/{shipId}")
    Call <Fleet> createShip(@Header("x-access-token") String token, @Path("shipId") int shipId,@Body HashMap<String, Integer> amount);

    @POST("/api/v1/buildings/create/{buildingId}")
    Call<Repo> createBuilding(@Header("x-access-token") String token, @Path("buildingId") int buildingId);
}
