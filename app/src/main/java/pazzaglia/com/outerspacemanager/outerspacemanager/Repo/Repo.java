package pazzaglia.com.outerspacemanager.outerspacemanager.Repo;

import java.util.List;

/**
 * Created by theo on 27/03/2018.
 */

public class Repo {
    private String token;
    private String expires;
    private double gas;
    private String gasModifier;
    private double minerals;
    private String mineralsModifier;
    private String points;
    private String username;
    private List<RepoBuilding> buildings;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpires() {
        return expires;
    }


    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getGas() {
        return gas;
    }

    public void setGas(double gas) {
        this.gas = gas;
    }

    public String getGasModifier() {
        return gasModifier;
    }

    public void setGasModifier(String gasModifier) {
        this.gasModifier = gasModifier;
    }

    public double getMinerals() {
        return minerals;
    }

    public void setMinerals(double minerals) {
        this.minerals = minerals;
    }

    public String getMineralsModifier() {
        return mineralsModifier;
    }

    public void setMineralsModifier(String mineralsModifier) {
        this.mineralsModifier = mineralsModifier;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public List<RepoBuilding> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<RepoBuilding> buildings) {
        this.buildings = buildings;
    }
}
