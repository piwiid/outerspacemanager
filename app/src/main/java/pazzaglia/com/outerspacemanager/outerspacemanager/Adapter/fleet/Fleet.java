package pazzaglia.com.outerspacemanager.outerspacemanager.Adapter.fleet;

import java.util.List;

import pazzaglia.com.outerspacemanager.outerspacemanager.Repo.RepoBuilding;

public class Fleet {

    private String size;

    private List<ListShips> ships;

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<ListShips> getShips() {
        return ships;
    }

    public void setShips(List<ListShips> ships) {
        this.ships = ships;
    }
}
